import hudson.tasks.Mailer
import hudson.FilePath
  
pipeline {
 	triggers {
 		bitbucketPush()
 	}
 	options {
 		buildDiscarder(logRotator(numToKeepStr: '300', daysToKeepStr:'300'))
 	}
 		
 	agent {
 		label 'LOCALHOST'
 	}
 
	
    stages {	
        stage('clean') {
            steps {
                cleanWs ()
            }
        }                
            
        stage('Checkout Bitbucket') {
            steps {
                checkout([$class: 'GitSCM', branches: scm.branches, doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: scm.userRemoteConfigs])
            }
        }

        stage('Step 1: Smoke Tests') {
           steps {
              script {
                
                    echo '====> step 1 OK'
                    echo '====> step 2 OK'
                    echo '====> step 3 OK'
                    echo '====> step 4 OK'
                    echo '<<<<= End of Step 1'
               
              }
            }
        }
 
        stage('Step 2: Healthcheck Tests') {
           steps {
              script {
                
                    echo '====> step 1 OK'
                    echo '====> step 2 OK'
                    echo '====> step 3 OK'
                    echo '====> step 4 OK'
                    echo '<<<<= End of Step 2'
               
              }
            }
        }
 
        stage('Step 3: Functional Tests') {
           steps {
              script {
                
                    echo '====> step 1 OK'
                    echo '====> step 2 OK'
                    echo '====> step 3 OK'
                    echo '====> step 4 OK'
                    echo '<<<<= End of Step 3'
               
              }
            }
        }

        stage('Step 4: Performance Tests') {
           steps {
              script {
                
                    echo '====> step 1 OK'
                    echo '====> step 2 OK'
                    echo '====> step 3 OK'
                    echo '====> step 4 OK'
                    echo '<<<<= End of Step 4'
               
              }
            }
        }

    }
    post {
        success { echo 'I succeeeded!' }
        unstable { echo 'I am unstable :/' }
        failure { echo 'Things were different before...' }
        changed { echo 'Things were different before...' }
    }
}

 	


